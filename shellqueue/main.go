package main

import (
	"flag"
	"fmt"
	"github.com/flowchartsman/boltqueue"
)

var (
	qdb  = flag.String("db", "db.db", "--db db.db")
	qadd = flag.String("add", "", "--add \"your string\"")
	qpop = flag.Bool("pop", false, "--pop")
)

func main() {
	flag.Parse()

	q, e := boltqueue.NewPQueue(*qdb)
	if e != nil {
		panic(e.Error())
	}

	if *qadd != "" {
		q.Enqueue(0, boltqueue.NewMessage(*qadd))
	}
	if *qpop {
		msg, e := q.Dequeue()
		if e == nil && msg != nil {
			fmt.Print(msg.ToString())
		}
	}
}
