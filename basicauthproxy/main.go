package main

import (
	"context"
	"flag"
	"io"
	"net"
	"net/http"
	"strings"
	"time"
)

var httpClient *http.Client

var listen = flag.String("l", ":8090", "-l 8090")
var backend = flag.String("b", "127.0.0.1:80", "-b 127.0.0.1:80")
var defaultHost = flag.String("h", "localhost", "-h hostname.tld")
var backendTimeout = flag.Int64("timeout", 10, "-timeout 10s")
var auth = flag.String("auth", "", "-auth user:password")

func main() {
	flag.Parse()

	dialer := &net.Dialer{
		Timeout:   time.Duration(*backendTimeout) * time.Second,
		KeepAlive: time.Duration(*backendTimeout) * time.Second,
		DualStack: true,
	}

	httpClient = &http.Client{
		Timeout: time.Duration(*backendTimeout) * time.Second,
		Transport: &http.Transport{
			DisableCompression: true,
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.DialContext(ctx, network, *backend)
			},
		},
	}

	caching := new(Proxy)

	http.ListenAndServe(*listen, caching)
}

type Proxy struct{}

func (c *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if *auth != "" {
		authParts := strings.Split(*auth, ":")
		if len(authParts) < 2 {
			authParts[1] = ""
		}
		user, pass, ok := r.BasicAuth()
		if user != authParts[0] || pass != authParts[1] || !ok {
			w.Header().Set("WWW-Authenticate", `Basic realm="Please login"`)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized\n"))
			return
		}
	}

	req, err := http.NewRequest(r.Method, r.RequestURI, r.Body)
	if err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}
	req.URL.Scheme = "http"
	req.URL.Host = *defaultHost

	cloneHeaders(&req.Header, &r.Header)

	req.Header.Del("Authorization")

	response, err := httpClient.Transport.RoundTrip(req)

	if err != nil {
		http.Error(w, "Timeout", http.StatusGatewayTimeout)
		return
	}
	defer response.Body.Close()

	wh := w.Header()
	cloneHeaders(&wh, &response.Header)

	w.WriteHeader(response.StatusCode)

	io.Copy(w, response.Body)
}

func cloneHeaders(dst *http.Header, src *http.Header) {
	for n, v := range *src {
		for _, vv := range v {
			dst.Add(n, vv)
		}
	}
}
